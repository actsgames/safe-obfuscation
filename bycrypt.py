"""

This file is the method that we have to encrypt and decrypt strings

"""

def obfuscate(byt):
    # Use same function in both directions.  Input and output are bytes
    # objects.
    encripted_key = b'__'
    lmask = len(encripted_key)
    return bytes(c ^ encripted_key[i % lmask] for i, c in enumerate(byt))

def des_obfuscate(byt):
    # Use same function in both directions.  Input and output are bytes
    # objects.
    desencripted_key = b'keyword'
    lmask = len(desencripted_key)
    return bytes(c ^ desencripted_key[i % lmask] for i, c in enumerate(byt))

def test(text):
    data = obfuscate(text.encode())
    #print(len(text), len(data), data)
    text_desobfuscated = des_obfuscate(data).decode()
    #print(text_desobfuscated)
    #print(text_desobfuscated == text)


simple_string = 'Just plain ASCII'

#test(simple_string)


"""
EXAMPLES
"""




"""
beteee = obfuscate('holis'.encode())
print(beteee)
z = list(beteee)
print(z)

for i in enumerate(z):
    print(bytes(i))
"""






"""
array = ["z", "y", "x", "w", "v", "u", "t", "s", "r", "q", "p", "o", "n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a"]
to = []

for i in range(len(array)):
    data = obfuscate(array[i].encode())
    to.append(list(data))

print(array)
print(to)
"""





"""
to = []
array1 = [[17], [18], [19], [28], [29], [30], [31], [24], [25], [26], [27], [4], [5], [6], [7], [0], [1], [2], [3], [12], [13], [14], [15], [8], [9], [10]]

print(len(array1))

for i in range(len(array1)):
    data = des_obfuscate(bytes(array1[i])).decode()
    to.append(data)

print(to)
"""