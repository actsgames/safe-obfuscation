# Safe Obfuscation

- Estamos usando la version de python3.7

## Como instalarlo

**1)** Ir al archivo *.bashrc* y agregar lo siguiente

```
    export PATH=$PATH:/path/to/safe-obfuscation
```

**2)** Ejecutar el comando ```so encrypt /path/to/proyect``` or ```so encrypt -f /path/to/file```