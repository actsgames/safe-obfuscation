#!/usr/bin/env python3
import sys
import os
import uuid

class Dictionary(object):
    def __init__():
        return

    UUID_KEY = str(uuid.uuid4()).encode()


class Utils(object):
    def __init__():
        return

    def obfuscate(byt):
        encripted_key = Dictionary.UUID_KEY
        lmask = len(encripted_key)
        return bytes(c ^ encripted_key[i % lmask] for i, c in enumerate(byt))