""" 
    Encrypt text
"""

__author__ = "martin.amengual@comunidad.ub.edu.ar"
import sys, os

os.chdir('..')
path_to_app = os.path.dirname(os.path.realpath(sys.argv[0]))

print(path_to_app)

sys.path.append(path_to_app)

from utils.add_method import *

## Constants

ALPHABET_ARRAY = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

ENCRYPTED_ALPHABET_ARRAY = [[37], [38], [39], [40], [41], [42], [43], [44], [45], [46], [47], [48], [49], [50], [51], [52], [53], [54], [55], [56], [57], [58], [59], [60], [61], [62]]

@add_method_builtin(list)
def get(self, i):
    return [bytes(c ^ b'__'[i % 2] for i, c in enumerate(bytes(self[j]))).decode() for j in range(26)][i]

def cifrar(txt):
    encripted_text = ""
    for t in txt:
        for i in range(len(ALPHABET_ARRAY)):
            if (t == ALPHABET_ARRAY[i]):
                encripted_text = encripted_text + ENCRYPTED_ALPHABET_ARRAY.get(i)
                break
            elif (len(ALPHABET_ARRAY)-1 == i):
                encripted_text = encripted_text + " "
    return encripted_text

def decifrar(txt):
    decrypted_text = ""
    for t in txt:
        for i in range(len(ENCRYPTED_ALPHABET_ARRAY)):
            if (t == ENCRYPTED_ALPHABET_ARRAY.get(i)):
                encripted_text = encripted_text + ALPHABET_ARRAY[i]
                break
            elif (len(ENCRYPTED_ALPHABET_ARRAY)-1 == i):
                encripted_text = encripted_text + " "
    return encripted_text

text = input("Introducir texto: ").lower()
text_cifrado = cifrar(text)
print("Texto cifrado:", text_cifrado)
print("Texto decifrado:", cifrar(text_cifrado))